﻿using System;
using System.Windows.Forms;

namespace BasicKeyboardAndMouseHook
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            IHook mouse = new HookMouse();
            mouse.SetHook();
            IHook keyboard = new HookKeyboard();
            keyboard.SetHook();
            Application.Run();
        }
    }
}
