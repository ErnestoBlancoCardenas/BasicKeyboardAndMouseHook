﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicKeyboardAndMouseHook
{
    public interface IHook
    {
        void Unhook();
        void SetHook();
    }
}
